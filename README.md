# IB _the data company_ API PHP Library

The official PHP library for using [the IB REST API](https://www.ib.nl/apidoc/).

The IB REST API provides access to an extended and up-to-date product catalog of construction materials. It also provides an EDI connection to the supply chain of many suppliers.

**Note 1:** The product catalog of IB _the data company_ is targeted to the Dutch en Belgium markets. Productinformation will therefore be mainly available in Dutch and most of the supplier are located in and around the Netherlands.

**Note 2:** It is completely feasible to use the IB REST API without this package. Please consult the [documentation](https://www.ib.nl/apidoc/) for an extended overview of the available endpoints our API provides.

Before using this library, you must have a valid API Key. To get an API Key, please contact IB _the data company_.

## Installation

The recommended way to install the IB API PHP Library is through composer.

```
# Install Composer
curl -sS https://getcomposer.org/installer | php
```

The IB API PHP Library requires php-http client (see [Setting up a Request Adapter](#setting-up-a-request-adapter)). There are several [providers](https://packagist.org/providers/php-http/client-implementation) available. If you were using guzzle6 your install might look like this.

```
composer require guzzlehttp/guzzle
composer require php-http/guzzle6-adapter
```

Next, run the Composer command to install the IB API PHP Library:

```
composer require ib/api-client
```

## Getting started

Because of dependency collision, we have opted to use a request adapter rather than
requiring a request library.  This means that your application will need to pass in
a request adapter to the constructor of the IB API PHP Library. Please visit the [HTTPlug](https://github.com/php-http/httplug) documentation for a list of supported [clients and adapters](http://docs.php-http.org/en/latest/clients.html). If you don't currently use a request library, you will need to require one and create a client from it and pass it along. The example below uses the GuzzleHttp Client Library.

A Client can be setup like so:

```php
<?php
require 'vendor/autoload.php';

use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use IB\API\Client as IBClient;

$httpClient = new GuzzleAdapter(new Client());
$apiClient = new IBClient($httpClient, [
    'key' => 'YOUR_API_KEY'
]);

?>
```

## Configuration
#### new \IB\API\Client(httpClient, options)
* `httpClient`
    * Required: Yes
    * HTTP client or adapter supported by HTTPlug
* `options`
    * Required: No
    * Type: `Array`
    * A valid IB API key or an array of options
* `options.key`
    * Required: Yes
    * Type: `String`
    * A valid IB API key
* `options.host`
    * Required: No
    * Type: `String`
    * Default: `www.ib.nl`
* `options.protocol`
    * Required: No
    * Type: `String`
    * Default: `https`
* `options.port`
    * Required: No
    * Type: `Number`
    * Default: 443
* `options.version`
    * Required: No
    * Type: `String`
    * Default: `v1`
* `options.debug`
    * Required: No
    * Type: `Boolean`
    * Default: `false`
    * If `debug` is true, then then all `IBApiResponse` and `IBApiException` instances will return any array of the request values through the function `getRequest`

## Methods
### request(method, uri [, payload [, headers]])
* `method`
    * Required: Yes
    * Type: `String`
    * HTTP method for request
* `uri`
    * Required: Yes
    * Type: `String`
    * The URI to recieve the request
* `payload`
    * Required: No
    * Type: `Array`
    * If the method is `GET` the values are encoded into the URL. Otherwise, if the method is `POST`, `PUT`, or `DELETE` the payload is used for the request body.
* `headers`
    * Required: No
    * Type: `Array`
    * Custom headers to be sent with the request.

### setHttpClient(httpClient)
* `httpClient`
    *  Required: Yes
    * HTTP client or adapter supported by HTTPlug

### setOptions(options)
* `options`
    *  Required: Yes
    *  Type: `Array`
    * See constructor

### Send An API Call Using The Base Request Function
We provide a base request function to access any of our API resources.
```php
<?php
require 'vendor/autoload.php';

use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use IB\API\Client as IBClient;

$httpClient = new GuzzleAdapter(new Client());
$apiClient = new IBClient($httpClient, [
    'key' => 'YOUR_API_KEY'
]);

$response = $apiClient->request('GET', '/');

?>
```

## Handling Responses
API calls using the `\IB\API\Client::request()` method return a `IBApiResponse` object.

```php
try {
    $response = $apiClient->request('GET', '/');

    echo $response->getStatusCode()."\n";
    print_r($response->getBody())."\n";
}
catch (\Exception $e) {
    echo $e->getCode()."\n";
    echo $e->getMessage()."\n";
}
```

## Handling Exceptions
An exception will be thrown in two cases: there is a problem with the request or the server returns a status code of `400` or higher.

### IBApiException
* **getCode()**
    * Returns the response status code of `400` or higher
* **getMessage()**
    * Returns the exception message
* **getBody()**
    * If there is a response body it returns it as an `Array`. Otherwise it returns `null`.
* **getRequest()**
    * Returns an array with the request values `method`, `url`, `headers`, `body` when `debug` is `true`

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
